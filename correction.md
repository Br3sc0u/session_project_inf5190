# Rapport Projet de session

## Pour le cours INF5190

### Tâches accomplis

- **A1**

  - Script pour lire les ressources depuis de le site de la ville. (script : `reader_xml.py`)  

   ```bash
  python reader_xml.py
  ```

  - Pour remplir la BD, initialement vide. Pas de besoin de spécifier le python car seulement python 3 installé dans l'venv.

- **A2**

  - Création de l'application Flask. Et affichage des contrevenants avec la fonctionnalité de recherche sur la page d'acceuil.

- **A3**

  - Mis en place d'un scheduler pour mis à jour de la base de donnée.

- **A4**

  - Création d'un service REST pour permettant d'obtenir la liste des contrevenants ayant commis une infraction entre deux dates spécifiées en paramètre. Les données retournées sont en format JSON.

- **A5**

  - Formulaire de recherche, avec requete Ajax, entre deux dates. Affiche le nom de l'établissement du contrevenants et leur nombre d'occurence trouvé pour ce dernier.

- **A6**

  - Formulaire de recherche, avec requete Ajax, par le choix d'une liste déroulante.  Possible de combiné la liste de déroulante avec les champs de dates de la tâche **A5** pour une recherche plus affiner.

- **B1**

  - Le système détecte les nouveaux contrevenants et l'envoi par courriel automatiquement. L'adresse du destinataire du courriel est stocké dans un fichier de configuration en format YAML.

    - Accès :

      - Compte Gmail : brescou.inf5190@gmail.com
      - Password : Triangle514
      - Lien : <https://twitter.com/rivemale_thomas>

- **B2**

  - Les noms des nouveaux contrevenants sont publiés automatiquement sur un compte Twitter.

  - Les accès sont les mêmes que la tâche précédente.

- **C1**

  - Le système offre un service REST permettant d'obtenir la liste des établissements ayant commis une ou plusieurs infractions. Pour chaque établissement, on indique le nombre d'infractions connues. La liste est triée en ordre décroissant du nombre d'infractions.
  Format `JSON`

- **C2**

  - Le système offre un service permettant d'obtenir exactement les mêmes données que le point C1. Format `XML`

- **C3**

  - Le système offre un service permettant d'obtenir exactement les mêmes données que le point C1. Format `CSV`

- **D1**

  - Mise en place d'un service de plainte. Où un utilisateur peut rediger une plainte. Création et utilisation d'un service rest pour l'implémentation de cette fonctionnalité qui permet de post une plainte.

- **F1**

  - Le système est entièrement déployé sur la plateforme infonuagique Heroku.

  - Accès compte:

    - Compte : brescou.inf5190@gmail.com
    - Password : Triangle_514
    - Lien heroku : <https://inf5190-projet.herokuapp.com/>

Tout les sercives **REST** sont documentés sur la route `/doc` comme il a été demandé.
