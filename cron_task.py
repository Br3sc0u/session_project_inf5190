import datetime
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import yaml

from .db import Db
from .reader_xml import get_elemtree_xml_root
from .utils import make_body_mail
from .bot_twitter import make_tweet


# Cron task daily with update of db and call for send tweet and mail
def update_db_daily():
    db = Db()
    root = get_elemtree_xml_root()
    if len(root) == db.get_length():
        print(f"Nothing to update on {datetime.datetime.now()}")
    else:
        id = db.get_last_id_record()
        for item in root:
            if not db.check_row_exist(item):
                id += 1
                db.add_new_data(item, id)
        send_mails(db.get_new_data())
        make_tweet()
        db.append_new_data_to_contrevenant()
        db.delete_data_to_new_contrevenant()
        print(f"Update completed on {datetime.datetime.now()}")


# take every mail from yaml file and send mail to each users
def send_mails(data):
    with open(r'email.yaml') as file:
        list_ = yaml.load(file, Loader=yaml.FullLoader)
        i = 1
        for et in list_:
            factory_mail(data, et[i]['name'], et[i]['mail'])
            i += 1


# create the structure of a mail and the authentification.
def factory_mail(data, name, address):
    src_address = 'brescou.inf5190@gmail.com'
    dst_address = address
    body = f"""Bonjour {name}!
    \n\n Voici la liste des nouveaux contrevenants:\n""" + make_body_mail(
        data)
    msg = MIMEMultipart()
    msg['Subject'] = 'Nouveaux contrevenants !'
    msg['From'] = src_address
    msg['To'] = dst_address
    msg.attach(MIMEText(body, 'plain'))
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(src_address, "Triangle514")
    text = msg.as_string()
    server.sendmail(src_address, dst_address, text)
    server.quit()
