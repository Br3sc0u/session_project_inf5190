import re
import datetime

complaint_schema = {
    "required": ["nameComplaint",
                 "firstNameComplaint",
                 "etablissement",
                 "dateVisit",
                 "address",
                 "town",
                 "description"],
    "type": "object",
    "properties": {
        "nameComplaint": {"type": "string", "minLength": 1},
        "firstNameComplaint": {"type": "string", "minLength": 1},
        "etablissement": {"type": "string", "minLength": 1},
        "dateVisit": {"type": "string", "format": "date"},
        "address": {"type": "string", "minLength": 1},
        "town": {"type": "string", "minLength": 1},
        "description": {"type": "string", "minLength": 1}
    },
    "additionalProperties": 'false'
}

current_year = datetime.datetime.now().year
regex = r"(([1|2]\d{3})\-((0[1-9])|(1[0-2]))\-([0-2][1-9]|(3[0-1])|([1-2]0)))"
date_validate_iso8601 = re.compile(regex).match


def validator_date(date):
    try:
        if date_validate_iso8601(date) is not None:
            date_year = int(date[:4])
            if date_year <= current_year:
                return True
    except ValueError:
        return False
