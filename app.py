from flask import Flask, render_template, request, url_for
from flask import redirect, session, jsonify, Response
from apscheduler.schedulers.background import BackgroundScheduler
from flask_json_schema import JsonSchema, JsonValidationError
import json

from .db import Db
from .cron_task import update_db_daily
from .validator import validator_date, complaint_schema
from .utils import *


app = Flask(__name__, static_folder="static")
app.secret_key = "#$c%U#e*D)L!c"
schema = JsonSchema(app)


@app.errorhandler(JsonValidationError)
def validation_error(e):
    jsonErr = {'error': e.message, 'errors': [
        validation_error.message for validation_error in e.errors]}
    return jsonify(jsonErr), 400


@app.route('/',
           methods=['GET', 'POST'])
def home():
    db = Db()
    if request.method == 'POST' and request.form.get("search") is not None:
        return redirect(url_for('result_search',
                                query_param=request.form.get("search")))
    else:
        liste_nom = db.get_nom_etablissement()
        return render_template("home.html",
                               liste_nom=liste_nom)

@app.route('/no_result',
           methods=['GET'])
def no_result():
    return render_template('no_result.html')


@app.route('/result_search',
           methods=['GET'])
def result_search():
    db = Db()
    search = db.search_from_bar(request.args['query_param']).fetchall()
    if not search:
        return redirect('/no_result')
    else:
        return render_template('search_result.html',
                               result_search=search)

@app.route('/api/contrevenants',
           methods=['GET'])
def rest_contrevenants_date():
    if 'du' in request.args and 'au' in request.args:
        date_start = request.args['du']
        date_end = request.args['au']
        if validator_date(date_start) and validator_date(date_end):
            date = (date_start, date_end)
            db = Db()
            data = db.get_data_from_range_date_rest(date)
            return jsonify(add_key_for_json_date(data),
                           200)
        else:
            return Response("{'Erreur' : 'date invalide'}",
                            status=400,
                            mimetype='appplication/json')
    else:
        return Response("{'Erreur' : 'parametre invalide'}",
                        status=400,
                        mimetype='appplication/json')


@app.route('/api/contrevenants/etablissements/<string:format_url>',
           methods=['GET'])
def rest_contrevenants_etablissement(format_url):
    db = Db()
    if format_url == 'json':
        return jsonify(add_key_for_json_etablissement_infraction(
            db.get_etablissement_infraction())), 200
    elif format_url == 'xml':
        rep = create_xml_from_db(
            db.get_etablissement_infraction())
        return Response(response=rep,
                        status=200,
                        content_type='text/xml; charset=utf-8',
                        mimetype='application/xml')
    elif format_url == 'csv':
        rep = create_csv_from_db(
            db.get_etablissement_infraction()).encode('utf-8')
        return Response(response=rep,
                        status=200,
                        content_type='text/csv; charset=utf-8',
                        mimetype='application/csv')
    else:
        return Response("{'Erreur' : 'format invalide'}",
                        status=400,
                        mimetype='appplication/json')


@app.route('/api/contrevenants/etablissement',
           methods=['GET'])
def get_etablissement_nom():
    db = Db()
    name = request.args['nom']
    data_etablissement = db.get_data_from_etablissement(
        name.upper())
    if data_etablissement:
        return jsonify(
            add_key_for_json_etablissement(
                data_etablissement)), 200
    else:
        return Response("{'Erreur' : 'Établissement n'existe pas'}",
                        status=404,
                        mimetype='appplication/json')


@app.route('/api/contrevenants/etablissement_date',
           methods=['GET'])
def get_etablissement_nom_with_date():
    db = Db()
    name = request.args['nom']
    date_start = request.args['du']
    date_end = request.args['au']
    if validator_date(date_start) and validator_date(date_end):
        data_etablissement_date = db.get_data_from_etablissement_date(
            name.upper(), date_start, date_end)
        if data_etablissement_date:
            return jsonify(
                add_key_for_json_etablissement(
                    data_etablissement_date)), 200
        else:
            return Response("{'Erreur' : 'Établissement n'existe pas'}",
                            status=404,
                            mimetype='appplication/json')
    else:
        return jsonify(Erreur = " Mauvais format date"),400
                


@app.route('/doc',
           methods=['GET'])
def print_doc():
    return render_template('api_raml.html')


@app.route("/api/add_complaint", methods=['POST'])
@schema.validate(complaint_schema)
def add_complaint():
    db = Db()
    complaint_data = request.get_json()
    complaint.id = db.get_length("complaint")
    complaint.name = complaint_data["nameComplaint"]
    complaint.first_name = complaint_data["firstNameComplaint"]
    complaint.etablissement = complaint_data["etablissement"]
    complaint.dateVisit = complaint_data["dateVisit"]
    complaint.address = complaint_data["address"]
    complaint.town = complaint_data["town"]
    complaint.description = complaint_data["description"]

    if(validator_date(complaint.dateVisit)):
        db.add_complaint(complaint)
        return jsonify(succees="Plainte creér avec succées."), 201
    else:
        return jsonify(erreur="dateVisit"), 400


@app.route('/complaint',
           methods=['GET'])
def complaint():
    return render_template('plainte.html')


def update_db(app=app):
    with app.app_context():
        update_db_daily()


if __name__ == 'session_project_inf5190.app':
    scheduler = BackgroundScheduler()
    scheduler.add_job(update_db, 'cron', hour=00, minute=00)
    scheduler.start()


if __name__ == 'app':
    scheduler = BackgroundScheduler()
    scheduler.add_job(update_db, 'cron', hour=00, minute=00)
    scheduler.start()
