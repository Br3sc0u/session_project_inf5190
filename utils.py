import datetime
import csv
from xml.etree import ElementTree as ET
from io import BytesIO, StringIO



month_dict = {'janvier': '01',
              'février': '02',
              'mars': '03',
              'avril': '04',
              'mai': '05',
              'juin': '06',
              'juillet': '07',
              'août': '08',
              'septembre': '09',
              'octobre': '10',
              'novembre': '11',
              'décembre': '12'
              }


# convert date to ISO date
def convert_date(date_to_convert):
    for key in month_dict:
        if key in date_to_convert:
            date_to_convert = date_to_convert.replace(key, month_dict[key])
            date_to_convert = date_to_convert.replace(' ', '/')
            date_to_convert = datetime.datetime.strptime(
                date_to_convert, '%d/%m/%Y').strftime('%Y-%m-%d')
    return date_to_convert


# make tuple of the data to simplify request sql in db.py
def convert_data_for_sql_research(item):
    return (item[3].text,
            item[1].text,
            convert_date(item[6].text),
            convert_date(item[7].text),
            item[5].text,
            item[2].text,
            item[8].text,
            item[0].text,
            item[4].text)


# Constructor body mail with all new contrevenants
def make_body_mail(data):
    body = ""
    for elem in data:
        body += f" - {str(elem[0]).title()} \n"
    return body


def add_key_for_json_date(data):
    return [{'id': item[0], 'adresse': item[1], 'categorie': item[2],
             'date_infraction': item[3], 'date_jugement': item[4],
             'description': item[5], 'nom': item[6],
             'montant': item[7], 'proprietaire': item[8],
             'ville': item[9]} for item in data]


def add_key_for_json_etablissement_infraction(data):
    return [{'etablissement : ': item[0],
             'nb_infraction':item[1]} for item in data]


# convert data from db to xml for rest service
def create_xml_from_db(data):
    doc = ET.Element('contrevenants')
    for elem in data:
        items = ET.SubElement(doc, 'contrevenant')
        etablissement = ET.SubElement(items, 'etablissement')
        nb_infraction = ET.SubElement(items, 'nb_infraction')
        etablissement.text = elem[0]
        nb_infraction.text = str(elem[1])
    et = ET.ElementTree(doc)
    file_xml = BytesIO()
    et.write(file_xml, encoding='utf-8', xml_declaration=True)
    return file_xml.getvalue()


# convert data from db to csv for rest service
def create_csv_from_db(data):
    file_csv = StringIO()
    writer = csv.writer(file_csv)
    writer.writerow(["etablissement", "nb_infraction"])
    for elem in data:
        writer.writerow([elem[0], str(elem[1])])
    return file_csv.getvalue()


def add_key_for_json_etablissement(data):
    return [{'adresse': item[1], 'categorie': item[2],
             'date_infraction': item[3], 'date_jugement': item[4],
             'description': item[5], 'nom': item[6].title(),
             'montant': item[7], 'proprietaire': item[8],
             'ville': item[9]} for item in data]


def create_complaint_dict_from_json(complaint_data):
        
        complaint.name = complaint_data["nameComplaint"]
        complaint.first_name = complaint_data["firstNameComplaint"]
        complaint.etablissement = complaint_data["etablissement"]
        complaint.dateVisit = complaint_data["dateVisit"]
        complaint.address = complaint_data["address"]
        complaint.town = complaint_data["town"]
        complaint.description = complaint_data["description"]
        return complaint