from flask import g
import sqlite3

from .utils import *


# database class to handle all the request for the db
class Db:
    def __init__(self):
        self.conn = None
        self.path = "Database/database.db"

    def get_db(self):
        db = getattr(g, "_database", None)
        if db is None:
            db = g._database = sqlite3.connect(self.path)
        return db

    def search_from_bar(self, search):
        conn = self.get_db()
        c = conn.cursor()
        sql_request = """ SELECT *
                            FROM contrevenants
                            WHERE (etablissement LIKE ?)
                            OR (proprietaire LIKE ?)
                            OR (adresse LIKE ?)
                            """
        data = ("%"+search+"%",
                "%"+search+"%",
                "%"+search+"%")
        return c.execute(sql_request,
                         data)

    def get_most_recent_date_judge(self):
        conn = self.get_db()
        c = conn.cursor()
        sql_request = """SELECT MAX(date_jugement)
                            FROM contrevenants
                      """
        return c.execute(sql_request).fetchone()[0].decode('utf-8')

    def get_length(self, table="contrevenants"):
        conn = self.get_db()
        c = conn.cursor()
        sql_request = f"""SELECT COUNT(*)
                            FROM {table}
                      """
        return c.execute(sql_request).fetchone()[0]

    def check_row_exist(self, data):
        conn = self.get_db()
        c = conn.cursor()
        sql_request = """ SELECT *
                            FROM contrevenants
                            WHERE (adresse = ?) AND
                                    (categorie = ?) AND
                                    (date_infraction = ?) AND
                                    (date_jugement = ?) AND
                                    (description_ = ?) AND
                                    (etablissement = ?) AND
                                    (montant = ?) AND
                                    (proprietaire = ?) AND
                                    (ville = ?)
                       """
        resultat = c.execute(sql_request,
                             convert_data_for_sql_research(data))
        return True if resultat.fetchall() else False

    def add_new_data(self, item, id):
        conn = self.get_db()
        c = conn.cursor()
        query = f"""INSERT INTO new_contrevenants
                    VALUES (?,?,?,?,?,?,?,?,?,?);
                """
        data = (id, item[3].text,
                item[1].text, convert_date(item[6].text),
                convert_date(item[7].text), item[5].text,
                item[2].text, item[8].text,
                item[0].text, item[4].text)
        c.execute(query, data)
        conn.commit()
        c.close()

    def get_last_id_record(self):

        conn = self.get_db()
        c = conn.cursor()
        sql_request = """ SELECT id
                                FROM    contrevenants
                                WHERE   id = (SELECT MAX(id)
                                    FROM contrevenants)
                        """

        id = c.execute(sql_request).fetchall()[0][0]
        return id

    def append_new_data_to_contrevenant(self):
        conn = self.get_db()
        c = conn.cursor()
        sql_request = """ INSERT INTO contrevenants
                                SELECT *
                                FROM    new_contrevenants
                        """
        c.execute(sql_request)
        conn.commit()
        c.close()

    def delete_data_to_new_contrevenant(self):
        conn = self.get_db()
        c = conn.cursor()
        sql_request = """ DELETE FROM new_contrevenants
                        """
        c.execute(sql_request)
        conn.commit()
        c.close()

    def get_data_from_range_date_rest(self, date):
        conn = self.get_db()
        c = conn.cursor()
        sql_request = f""" SELECT *
                                FROM contrevenants
                                WHERE (date_infraction >= ?
                                AND date_infraction <= ?)
                                """
        return c.execute(sql_request, date).fetchall()

    def get_new_data(self):
        conn = self.get_db()
        c = conn.cursor()
        sql_request = f""" SELECT DISTINCT etablissement
                                FROM new_contrevenants
                                """
        return c.execute(sql_request).fetchall()

    def get_etablissement_infraction(self):
        c = self.get_db().cursor()
        sql_request = """ SELECT etablissement , COUNT (etablissement) AS nb
                            FROM contrevenants
                            GROUP BY etablissement
                            ORDER BY nb DESC
                            """
        return c.execute(sql_request).fetchall()

    def get_nom_etablissement(self):
        c = self.get_db().cursor()
        sql_request = """ SELECT DISTINCT etablissement
                            FROM contrevenants
                            ORDER BY etablissement ASC
                            """
        return c.execute(sql_request).fetchall()

    def get_data_from_etablissement(self, name):
        c = self.get_db().cursor()
        sql_request = """ SELECT *
                            FROM contrevenants
                            WHERE (etablissement = ?)
                       """
        return c.execute(sql_request, (name,)).fetchall()

    def get_data_from_etablissement_date(self, name, date_start, date_end):
        c = self.get_db().cursor()
        sql_request = """ SELECT *
                            FROM contrevenants
                            WHERE etablissement = ? AND
                                date_infraction >= ? AND
                                date_infraction <= ?
                                """
        return c.execute(sql_request,
                         (name, date_start, date_end,)).fetchall()

    def add_complaint(self, dict_comp):
        conn = self.get_db()
        c = conn.cursor()
        sql_request = """INSERT INTO complaint
                        VALUES (?,?,?,?,?,?,?,?);
                        """
        data = (dict_comp.id, dict_comp.etablissement,
                dict_comp.address, dict_comp.town,
                dict_comp.dateVisit, dict_comp.name,
                dict_comp.first_name, dict_comp.description)
        c.execute(sql_request, data)
        conn.commit()
        c.close()
