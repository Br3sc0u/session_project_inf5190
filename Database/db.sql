CREATE TABLE contrevenants (
  id INTEGER PRIMARY KEY,
  adresse VARCHAR(200),
  categorie VARCHAR(100),
  date_infraction TEXT,
  date_jugement TEXT,
  description_ VARCHAR(500),
  etablissement VARCHAR(100),
  montant INTEGER,
  proprietaire VARCHAR(100),
  ville VARCHAR(100)
);

CREATE TABLE new_contrevenants (
  id INTEGER PRIMARY KEY,
  adresse VARCHAR(200),
  categorie VARCHAR(100),
  date_infraction TEXT,
  date_jugement TEXT,
  description_ VARCHAR(500),
  etablissement VARCHAR(100),
  montant INTEGER,
  proprietaire VARCHAR(100),
  ville VARCHAR(100)
);

CREATE TABLE complaint(
  id INTEGER PRIMARY KEY,
  etablissement VARCHAR(100),
  adresse VARCHAR(200),
  ville VARCHAR(100),
  date TEXT,
  nom VARCHAR(100),
  prenom VARCHAR(100),
  description_ VARCHAR(100)
  );
