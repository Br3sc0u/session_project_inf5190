function ajaxDate () {
  let dateDu = document.getElementById('du').value,
    dateAu = document.getElementById('au').value,
    inputEmpty = dateAu == '' && dateDu == '',
    select_default =
      document.getElementById('select_nom').value == "Nom d'établissement"
  cleanTable()

  if (!inputEmpty || !select_default) {
    let xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function () {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          let jsonData = JSON.parse(xhr.responseText)
          if (!inputEmpty && select_default) {
            document.getElementById('du').nextElementSibling.hidden = true
            document.getElementById('au').nextElementSibling.hidden = true
            document.getElementById('target_A5').hidden = false
            data = jsonCalculResto(jsonData[0])
            addRowHtmlTableA5(data)
          } else {
            document.getElementById('du').nextElementSibling.hidden = true
            document.getElementById('au').nextElementSibling.hidden = true
            document.getElementById('target_A6').hidden = false
            addRowHtmlTableA6(jsonData)
          }
        } else if (xhr.status === 400) {
          document.getElementById('du').nextElementSibling.hidden = false
          document.getElementById('au').nextElementSibling.hidden = false
        } else {
          console.log('Error from server')
        }
      }
    }
    if (!inputEmpty && select_default) {
      xhr.open('GET', '/api/contrevenants?du=' + dateDu + '&au=' + dateAu, true)
      xhr.send()
    } else if (inputEmpty && !select_default) {
      nom = document.getElementById('select_nom').value
      xhr.open(
        'GET',
        '/api/contrevenants/etablissement?nom=' + encodeURIComponent(nom),
        true
      )
      xhr.send()
    } else if (!inputEmpty && !select_default) {
      nom = document.getElementById('select_nom').value
      xhr.open(
        'GET',
        '/api/contrevenants/etablissement_date?du=' +
          dateDu +
          '&au=' +
          dateAu +
          '&nom=' +
          encodeURIComponent(nom),
        true
      )
      xhr.send()
    } else {
      console.log('Erreur')
    }
  }
}

function addRowHtmlTableA5 (data) {
  for (let index = 0; index < data.length; index++) {
    let element = data[index],
      table = document.getElementById('table_A5'),
      newRow = table.insertRow(0),
      newCellName = newRow.insertCell(0),
      newCellOcc = newRow.insertCell(1),
      name = document.createTextNode(element.nom),
      occ = document.createTextNode(element.occ)

    newCellName.appendChild(name)
    newCellOcc.appendChild(occ)
  }
  $(document).ready(function () {
    $('#table_pagination_A5').DataTable({
      pagingType: 'simple',
      retrieve: true
    })
  })
}
function addRowHtmlTableA6 (data) {
  for (let index = 0; index < data.length; index++) {
    let element = data[index],
      table = document.getElementById('table_A6'),
      newRow = table.insertRow(0),
      newCellName = newRow.insertCell(0),
      newCelladresse = newRow.insertCell(1),
      newCellVille = newRow.insertCell(2),
      newCellCat = newRow.insertCell(3),
      newCellJug = newRow.insertCell(4),
      newCellInf = newRow.insertCell(5),
      newCellProp = newRow.insertCell(6),
      newCellMont = newRow.insertCell(7),
      newCellDescrip = newRow.insertCell(8),
      name = document.createTextNode(element.nom),
      adresse = document.createTextNode(element.adresse),
      ville = document.createTextNode(element.ville),
      cat = document.createTextNode(element.categorie),
      jug = document.createTextNode(element.date_jugement),
      inf = document.createTextNode(element.date_infraction),
      pro = document.createTextNode(element.proprietaire),
      mont = document.createTextNode(element.montant),
      description = document.createTextNode(element.description)

    newCellName.appendChild(name)
    newCelladresse.appendChild(adresse)
    newCellVille.appendChild(ville)
    newCellCat.appendChild(cat)
    newCellJug.appendChild(jug)
    newCellInf.appendChild(inf)
    newCellProp.appendChild(pro)
    newCellMont.appendChild(mont)
    newCellDescrip.appendChild(description)
  }
  $(document).ready(function () {
    $('#table_pagination_A6').DataTable({
      pagingType: 'simple',
      retrieve: true
    })
  })
}
function jsonCalculResto (jsonData) {
  let find = true,
    tab = [],
    tabA5Object = {},
    i = 0
  for (let index = 0; index < jsonData.length; index++) {
    let element = jsonData[index]
    if (tab.length == 0) {
      tabA5Object.occ = 1
      tabA5Object.nom = element.nom
      find = false
      tab.push(tabA5Object)
    } else {
      i = 0
      find = true
    }
    while (find && i < tab.length) {
      tabA5Object = { occ: 0 }
      if (element.nom === tab[i].nom) {
        tab[i].occ += 1
        find = false
      } else if (i == tab.length - 1) {
        tabA5Object.nom = element.nom
        tabA5Object.occ += 1
        tab.push(tabA5Object)
        find = false
      }
      i++
    }
  }
  return tab
}

function cleanTable () {
  if ($.fn.dataTable.isDataTable('#table_pagination_A5')) {
    $('#table_pagination_A5')
      .DataTable()
      .clear()
      .destroy()
  } else if ($.fn.dataTable.isDataTable('#table_pagination_A6')) {
    $('#table_pagination_A6')
      .DataTable()
      .clear()
      .destroy()
  }
}

function sendComplaint () {
  let jsonDataComplaint = new Object(),
    fields = [
      'nameComplaint',
      'firstNameComplaint',
      'etablissement',
      'dateVisit',
      'address',
      'town',
      'description'
    ]
  if (requiredFields(fields)) {
    fields.forEach(id => {
      jsonDataComplaint[id] = document.getElementById(id).value
    })
    let xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function () {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 201) {
          Swal.fire({
            icon: 'success',
            title: 'Reçu !',
            text: "Plainte créée ! Et on retourne sur l'accueil !",
            showConfirmButton: false
          })
          setTimeout(function () {
            window.location.href = '/'
          }, 4000)
        } else {
          let jsonError = JSON.parse(xhr.responseText)
          console.log(jsonError.erreur)
          errorFieldComplaint(jsonError.erreur)
        }
      }
    }
    xhr.open('POST', '/api/add_complaint', true)
    xhr.setRequestHeader('Content-Type', 'application/json')
    xhr.send(JSON.stringify(jsonDataComplaint))
  }
}
function errorFieldComplaint (error) {
  document.getElementById(error).nextElementSibling.innerHTML =
    'Format non respecté'
  document.getElementById(error).nextElementSibling.hidden = false
}
function requiredFields (tab) {
  let fieldNotEmpty = true
  tab.forEach(id => {
    if (document.getElementById(id).value == '') {
      fieldNotEmpty = false
      document.getElementById(id).nextElementSibling.hidden = false
    } else if (document.getElementById(id).nextElementSibling.hidden == false) {
      document.getElementById(id).nextElementSibling.hidden = true
    }
  })
  return fieldNotEmpty
}
