# -*- coding: utf-8 -*-

import requests
from xml.etree import ElementTree as ET
import sqlite3

url = "http://donnees.ville.montreal.qc.ca/"\
    "dataset/a5c1f0b9-261f-4247-99d8-f28da5000688/"\
    "resource/92719d9b-8bf2-4dfd-b8e0-1021ffcaee2f/"\
    "download/inspection-aliments-contrevenants.xml"
id = 0


# get the root of the element tree xml
def get_elemtree_xml_root():

    response = requests.get(url)
    root = ET.fromstring(response.content)
    return root


# script for full db
if(__name__ == "__main__"):
    from utils import convert_date

    for item in get_elemtree_xml_root():

        id += 1
        conn = sqlite3.connect("database/database.db")
        c = conn.cursor()
        query = f"""INSERT INTO contrevenants
                    VALUES (?,?,?,?,?,?,?,?,?,?);
                """
        data = (id, item[3].text,
                item[1].text, convert_date(item[6].text),
                convert_date(item[7].text), item[5].text,
                item[2].text, item[8].text,
                item[0].text, item[4].text)
        c.execute(query, data)
        conn.commit()
        c.close()
