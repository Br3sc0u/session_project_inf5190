# Pour compiler en local

Saisir les commandes suivantes :

```bash
source env/bin/activate
make
```

Ouvrir le localhost au *port 5000* sur le fureteur de votre choix.

Initialement la base de donnée est vide, cependant vous pouvez la remplir avec la commande suivante:

```bash
make db
```